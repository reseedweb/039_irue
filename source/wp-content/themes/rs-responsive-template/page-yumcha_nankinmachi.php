<?php get_header(); ?>
    <div class="primary-row container clearfix"><!-- begin container -->                    
        <div class="row clearfix">
            <div class="col-lg12 col-md-12 col-sm-12 col-xs-12">
                <div class="workshop-main1">										
					<h3 class="workshop-main1-title">飲茶 香港 南京町店</h3>
					<div class="workshop-main1-text1">
						<p>「西安門」からすぐの飲茶 香港 南京町店では飲茶盛り合わせセットがおススメです。<br />
						中国茶セットはチマキがもっちもっちで具沢山！セットの内容も種類が豊富でお得です。<br />
						2Fには、テーブル席と、円卓の中華テーブルがありますのでゆったりと南京町でランチを堪能できます。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row container clearfix"><!-- begin container -->                    
		<div class="row clearfix">
			<div class="col-lg6 col-md-6 col-sm-6 col-xs-12 mb20">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/nankinmachi_content_top.jpg" alt="南京町店" /></p>
			</div>

            			<div class="col-lg6 col-md-6 col-sm-6 col-xs-12">
				<h4 class="workshop-title1">街を歩けば</h4>
				<div class="workshop-text1">
					<p>神戸南京町は100軒を超す店舗が並ぶ関西一のチャイナタウン。<br />
					中華食材、雑貨、料理など多彩な店舗が軒を連ねる人気の観光エリアで、JR・阪神元町駅東口より歩いて数分という好立地にあります。<br />
					横浜中華街、長崎新地中華街とともに日本三大チャイナタウンの一つに数えられるます。</p>
				</div>
				<!-- <h5 class="workshop-title2">交通手段</h5>
				<div class="workshop-text2">
					<p>JR元町駅南口より徒歩、元町駅（阪神）から194m</p>
				</div>
				<h5 class="workshop-title2">料理内容</h5>
				<div class="workshop-text2">
					<p>中華料理、飲茶・点心</p>
				</div>
				<h5 class="workshop-title2">営業時間</h5>
				<div class="workshop-text2">
					<p>10：30～21：30　ランチ営業、日曜営業（定休日水曜日）</p>
				</div>
				<h5 class="workshop-title2">予算</h5>
				<div class="workshop-text2">
					<p>￥1,000～￥1,999</p>
				</div> -->
			</div>	
		</div>
			<div class="col-lg12 col-md-12">
				<table class="company-table-content">
					<tbody>
						<tr>
							<th>店名</th>
							<td>飲茶香港 （ヤムチャホンコン）</td>
						</tr>
						<tr>
							<th>所在地</th>
							<td>〒650-0023<br>兵庫県神戸市中央区栄町通2丁目9-6</td>
						</tr>
						<tr>
							<th>TEL</th>
							<td>078-393-5838</td>
						</tr>
						<tr>
							<th>料理内容</th>
							<td>中華料理、飲茶・点心</td>
						</tr>
						<tr>
							<th>交通手段</th>
							<td>
							<p>交通手段:JR元町駅南口より徒歩、元町駅（阪神）から194m</p>
							</td>
						</tr>
					</tbody>
				</table>

				<div class="workshop-map" id="map_area">
					 <script type="text/javascript">
						$(function() {
							map_insert (34.6877554,135.1870704);
						});
					</script>
				</div>
			</div>
	</div><!-- end primary-row --> 
	
	<div class="primary-row container clearfix"><!-- begin container -->                    
		<div class="row clearfix">
            <div class="col-md-12">
				<h3 class="workshop-slider-title">飲茶 香港の特徴</h3>
				<?php get_template_part('part','characteristics');?>
			</div>						
		</div> 
	</div><!-- end primary-row --> 
	
	<?php get_template_part('part','dishes') ;?>                                                     		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>