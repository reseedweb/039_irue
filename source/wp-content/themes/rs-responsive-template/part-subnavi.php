<div class="sub-navi-info">
	<div class="container clearfix">
		<div class="sub-navi row clearfix">
			<ul>
				<li><a href="<?php bloginfo('url'); ?>/greeting">ご挨拶</a></li>
				<li><a href="<?php bloginfo('url'); ?>/company">会社紹介</a></li>
				<li><a href="<?php bloginfo('url'); ?>/history">会社沿革</a></li>
				<li><a href="<?php bloginfo('url'); ?>/quality">品質環境方針</a></li>				
			</ul>
			<div class="sub-navi-space"><img src="<?php bloginfo('template_url'); ?>/img/common/subnavi_line.jpg" alt="subnavi" /></div>
		</div>
	</div>
</div><!-- end sub-navi-content -->