			<div id="page-feature">						
				<div class="page-feature-content clearfix">					
					<?php if(is_page('yumcha_nankinmachi')) : ?>		
						<img src="<?php bloginfo('template_url'); ?>/img/content/nankinmachi_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<div class="row clearfix"><!-- begin row -->
								<div class="col-md-12">
									<h2 class="feature-title"><?php the_title(); ?></h2>
								</div>						
							</div><!-- end row -->
						</div><!-- end container -->					
						
					<?php elseif(is_page('chinese_portopia')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/nankinmachi_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<div class="row clearfix"><!-- begin row -->
								<div class="col-md-12">
									<h2 class="feature-title"><?php the_title(); ?></h2>
								</div>						
							</div><!-- end row -->
						</div><!-- end container -->
						
					<?php elseif(is_page('company')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/company_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<div class="row clearfix"><!-- begin row -->
								<div class="col-md-12">
									<h2 class="feature-title"><?php the_title(); ?></h2>
								</div>						
							</div><!-- end row -->
						</div><!-- end container -->
						
					<?php elseif(is_page('contact')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/company_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<div class="row clearfix"><!-- begin row -->
								<div class="col-md-12">
									<h2 class="feature-title"><?php the_title(); ?></h2>
								</div>						
							</div><!-- end row -->
						</div><!-- end container -->					
					<?php endif; ?>  
				</div><!-- end page-feature-content -->
			</div><!-- end page-feature -->   
		
			<?php get_template_part('part','breadcrumb');?>