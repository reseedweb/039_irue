<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="sideBanner">
        <div class="sideBanner_contact_bg">
			<img src="<?php bloginfo('template_url'); ?>/img/common/sideBanner_contact_bg.jpg" alt="sideBanner_contact_img.jpg" />
            <div class="sideBanner_contact_img">
                <p>
                    <a href="<?php bloginfo('url'); ?>/contact">
                        <img src="<?php bloginfo('template_url'); ?>/img/common/sideBanner_contact_img1.jpg" alt="sideBanner_contact_img.jpg" />
                    </a>                
                </p>
                <p>
                    <a href="<?php bloginfo('url'); ?>/#">
                        <img src="<?php bloginfo('template_url'); ?>/img/common/sideBanner_contact_img2.jpg" alt="sideBanner_contact_img.jpg" />
                    </a>                
                </p>
            </div>
        </div>
    </div>    
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="sideBanner">                                    
        <p><a href="<?php bloginfo('url'); ?>/#"><img src="<?php bloginfo('template_url'); ?>/img/common/sideBanner_img1.jpg" alt="sideBanner_img1.jpg" /></a></p>
		<p><a href="<?php bloginfo('url'); ?>/#"><img src="<?php bloginfo('template_url'); ?>/img/common/sideBanner_img1.jpg" alt="sideBanner_img1.jpg" /></a></p>
    </div>
</div><!-- end sidebar-row -->   
                                                                              
<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<div class="sideBlog">
		<h3>最新の投稿</h3>
		<ul>
			<?php query_posts("posts_per_page=5"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
			<li>
			<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
			</li>
			<?php endwhile;endif; ?>
		</ul> 
	</div>
	<div class="sideBlog">
	    <h3>カテゴリ</h3>
	      <ul class="category">
			<?php wp_list_categories('sort_column=name&optioncount=0&hierarchical=1&title_li='); ?>
	      </ul>                             	
	</div>
	<div class="sideBlog">
      <h3>アーカイブ</h3>
      <ul><?php wp_get_archives('type=monthly&limit=12'); ?></ul>                               	
	</div>
</div><!-- end sidebar-row -->
