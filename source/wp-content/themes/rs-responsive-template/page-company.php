<?php get_template_part('header'); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="container clearfix">
		<div class="row clearfix">
			<div class="col-md-12">
				<p class="mb20"><img src="<?php bloginfo('template_url'); ?>/img/content/company_content_img.jpg" alt="company" /></p>	
				<!-- <h3 class="h3-title">企業理念</h3>
				<div class="company-text">
					<p>企業理念が入ります。
					</p>
				</div> -->
			</div>
		</div>
	</div>	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="container clearfix">
		<div class="row clearfix">
			<div class="col-lg12 col-md-12">
				<h3 class="h3-title">会社概要</h3>				
				<table class="company-table-content">
					<tbody>
						<tr>
							<th>社名</th>
							<td>株式会社 イルエ</td>
						</tr>
						<tr>
							<th>所在地</th>
							<td>〒650-0023<br>兵庫県神戸市中央区栄町通2丁目9-6</td>
						</tr>
						<tr>
							<th>TEL</th>
							<td>078-393-5838</td>
						</tr>
						<tr>
							<th>E-mail</th>
							<td></td>
						</tr>
						<tr>
							<th>代表者</th>
							<td>林　虎庭</td>
						</tr>
						<tr>
							<th>料理内容</th>
							<td>中華料理、飲茶・点心</td>
						</tr>
						<tr>
							<th>店舗情報</th>
							<td>
								<p>
								■ 飲茶香港 （ヤムチャホンコン）<br />
								〒650-0023　兵庫県神戸市中央区栄町通2-9-6<br />
								TEL:078-393-5838
								</p>
								<p class="pt20">
								■ 好好中華(ハウハウチュウカ)<br />
								〒650-0046  兵庫県神戸市中央区港島中町3-2-6 グルメポートアイランド店1F<br />
								TEL:078-302-5721
								</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>	
		</div>
	</div>					
</div><!-- end primary-row -->
<?php get_template_part('footer'); ?>