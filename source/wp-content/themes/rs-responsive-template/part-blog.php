<div class="primary-row clearfix">
	<h2 class="clearfix">Part Blog
	<a class="btn btn-default btn-sm pull-right" href="<?php bloginfo('url'); ?>/blog">View All Posts</a></h2>
	<div class="primary-row-content row clearfix">		
		<?php query_posts("posts_per_page=3"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18">
			<p class="text-center"><a href="<?php the_permalink(); ?>">
			<?php if(has_post_thumbnail()) : ?>
			<?php echo get_the_post_thumbnail(get_the_id(), 'full'); ?>			
			<?php else : ?>
			<?php endif; ?>
			</a></p>
			<p class="text-center pt10"><?php the_title(); ?></p>
		</div>
		<?php endwhile; endif; ?>		
	</div>
</div>