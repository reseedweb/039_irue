<?php get_header(); ?>
    <div class="container"><!-- begin container -->                    
        <div class="row message-row clearfix">
            <div class="col-lg3 col-md-3 col-sm-3 col-xs-6 message-col">
                <article class="top-main1 clearfix">
					<h2 class="top-main1-title">お節料理</h2>
					<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img1.jpg" alt="top" /></div>
					<div class="top-main1-text">忘年会、新年会、大小宴会承ります。</div>
				</article>
            </div>			
			
			<div class="col-lg3 col-md-3 col-sm-3 col-xs-6 message-col">
                <article class="top-main1 clearfix">
					<h2 class="top-main1-title">当店自慢の餃子</h2>
					<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img2.jpg" alt="top" /></div>
					<div class="top-main1-text">パリッと香ばしい餃子はご好評いただいています。</div>
				</article>
            </div>
			
			<div class="col-lg3 col-md-3 col-sm-3 col-xs-6">
                <article class="top-main1 clearfix">
					<h2 class="top-main1-title">中華セットメニュー</h2>
					<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img3.jpg" alt="top" /></div>
					<div class="top-main1-text">セットメニューは豊富な種類をご用意しています。</div>
				</article>
            </div>
			
			<div class="col-lg3 col-md-3 col-sm-3 col-xs-6">
                <article class="top-main1 clearfix">
					<h2 class="top-main1-title">単品メニューも豊富</h2>
					<div class="top-main1-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_img4.jpg" alt="top" /></div>
					<div class="top-main1-text">彩り豊かで食欲をそそるのが中華料理の魅力。</div>
				</article>
            </div>
        </div>        
	</div><!-- end primary-row --> 
	
    <div class="primary-row container clearfix"><!-- begin primary-row -->                                                                               
        <div class="row clearfix">
            <div class="col-md-12">
                <p class="top-status">神戸南京町とポートピアで本格中華を堪能。<br>南京町で本格中華料理を食べたい方は是非お立ち寄りください。</p>
            </div>
        </div>        
	</div><!-- end primary-row -->                                                                         
	
	
	<div class="primary-row container clearfix"><!-- begin container -->                    
        <div class="row top-main2-content clearfix">
            <div class="col-lg6 col-md-6 col-sm6 col-xs-6 top-main2-infotext">
                <article class="top-main2">
					<h2 class="top-main2-title">飲茶 香港 南京町店</h2>					
					<div class="top-main2-text">
						<p>「西安門」からすぐの飲茶 香港 南京町店では飲茶盛り合わせセットがおススメです。<br />
						中国茶セットはチマキがもっちもっちで具沢山！セットの内容も種類が豊富でお得です。<br />
						２Ｆには、テーブル席と、円卓の中華テーブルがありますのでゆったりと南京町でランチを堪能できます。</p>
					</div>
				</article>
            </div>			
			<div class="col-lg6 col-md-6 col-sm6 col-xs-6 top-main2-infoimg">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_main2_img1.jpg" alt="飲茶 香港 南京町店" />
			</div>			
        </div>        
	</div><!-- end primary-row --> 
	
	<div class="primary-row container clearfix"><!-- begin container -->                    
        <div class="row clearfix">
			<div class="col-lg6 col-md-6 col-sm6 col-xs-6 top-main2-infoimg">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_main2_img2.jpg" alt="好好中華　ポートピア店" />
			</div>	
            <div class="col-lg6 col-md-6 col-sm6 col-xs-6 top-main2-infotext">
                <article class="top-main2">
					<h2 class="top-main2-title">好好中華　ポートピア店</h2>					
					<div class="top-main2-text">
						<p>好好中華ポートピア店のテキストが入ります。<br>
					好好中華ポートピア店のテキストが入ります。好好中華ポートピア店のテキストが入ります。<br>
					好好中華ポートピア店のテキストが入ります。好好中華ポートピア店のテキストが入ります。好好中華ポートピア店のテキストが入ります。</p>
					</div>
				</article>
			</div>						
        </div>        
	</div><!-- end primary-row --> 
	
	<div class="primary-row container clearfix"><!-- begin primary-row -->                                                                         
		<div class="row clearfix">
			<div class="col-md-12">
				<h2 class="top-h2">お知らせ<span class="top-rss"><a href="<?php bloginfo('url'); ?>">RSS</a></span></h2>
			</div>					
		</div>
		<div class="row clearfix">
				<?php
					//http://www.wpbeginner.com/beginners-guide/what-why-and-how-tos-of-creating-a-site-specific-wordpress-plugin/
					$loop = new WP_Query('showposts=5&orderby=ID&order=DESC');
					if($loop->have_posts()):
				?>
				<ul class="top-blog">
				<?php while($loop->have_posts()): $loop->the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>">	
							<div class="top-blog-info">
								<div class="col-lg2 col-md-2 col-sm12 col-xs-12 top-blog-date"><?php the_time('Y/m/d'); ?></div>
								<div class="col-lg10 col-md-10 col-sm12 col-xs-12 top-blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>																	
								<i class="fa fa-angle-right"></i>
							</div>
						</a>
					</li>                              
				<?php endwhile; wp_reset_postdata();?>                        
				<?php else: ?>
					<li>No recent posts yet!</li>
				<?php endif; ?>		
				</ul>	 			
		</div>
		<div class="row clearfix">
			<div class="col-md-12">
				<div class="top-more-blog"><a href="<?php bloginfo('url'); ?>">お知らせ一覧</a></div>
			</div>			
		</div>
	</div><!-- end primary-row --> 
	
	<div id="top-bottom" class="primary-row clearfix"><!-- begin primary-row -->                                                                         
		<article class="top-main3">
			<div class="top-main3-content">
				<div class="container clearfix">
					<div class="row clearfix">
						<div class="col-md-12">												
							<div class="top-main3-title">中国人料理人による本格中華が味わえます。</div>
							<div class="top-main3-text">
								<p>リーズナブルに、本格的な中国粥や飲茶が食べられて、店内で食事していると、まるで「本場」で食事をしている気分になれます。<br />
								飲茶類は数種類を一度に楽しめ、セットの炒飯もぱらっと仕上がっていて最高です。</p>
							</div>
						</div>					
					</div>
				</div>
			</div>					
		</article>
		
		<article class="top-main4">
			<div class="container clearfix">
				<div class="row clearfix">
					<div class="col-md-12">
						<div class="top-main4-title">お店でゆっくり食べるのも良し、食べ歩きも良し、南京町にお越しの際は是非、飲茶 香港 南京町店にお越しください。</div>
						<div class="top-main4-text">
							<p>神戸南京町は100軒を超す店舗が並ぶ関西一のチャイナタウン。<br />
					<!-- 中華食材、雑貨、料理など多彩な店舗が軒を連ねる人気の観光エリアで、JR・阪神元町駅東口より歩いて数分という好立地にあります。<br />
					横浜中華街、長崎新地中華街とともに日本三大チャイナタウンの一つに数えられるます。 --></p>
						</div>						
					</div>					
				</div>
			</div>		
			<div class="top-main-slider">
				<?php	
					$posts = get_posts(array(
						'post_type'=> 'top',				
					));?>			
					
					<?php
					$i = 0;
					foreach($posts as $post):
					?>	
					<?php $i++; ?>
						<div>
							<?php 
							if ( has_post_thumbnail() ) {
								$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
								echo '<a rel="lightbox" href="' . $large_image_url[0] . '" title="' . the_title_attribute( 'echo=0' ) . '" >';
								echo '<img src="'. $large_image_url[0].'" title="' . the_title_attribute( 'echo=0' ) . '">';
								echo '</a>';
							}
							?>
						</div><!-- end item -->            
				<?php endforeach; ?>
				
			</div>
		</article>		
		
		<div class="workshop-map" id="map_area">
			<script>
				$(function() {
					map_insert (34.6877554,135.1870704);
				});
			</script>
		</div>	             
	</div><!-- end primary-row -->
	                                                                		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>