<div class="primary-row container clearfix"><!-- begin container -->                    
	<div class="row clearfix">
        <div class="col-lg12 col-md-12 col-sm-12 col-xs-12">
			<h3 class="pladan-title">好好 中華の料理一例"</h3>
		</div>
	</div>
	<?php		
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$menucn_posts = new WP_Query( array( 'post_type' => 'menucn', 'posts_per_page' => 6, 'paged'=>$paged ) ); 
	?>  		
	<?php $i = 0;?>
	<?php while ( $menucn_posts->have_posts() ) : $menucn_posts->the_post(); ?>
	<?php $i++; ?>
	<?php if($i==1) : ?>
	<div class="row dishes-row clearfix">
	<?php endif; ?>	
		<div class="col-lg4 col-md-4 col-sm4 col-xs-4 pladan-main3-sm">
			<div class="pladan-main3">				
				<div class="pladan-main3-img"><?php echo get_the_post_thumbnail($menucn_posts->ID,'medium'); ?></div>
				<h4 class="pladan-main3-title"><?php the_title(); ?></h4>
				<div class="pladan-main3-text">
					<p>￥<?php the_field('price'); ?></p>
				</div>
			</div>
		</div>
	<?php if($i%3 == 0) : ?>					
	</div>	
	<?php endif; ?>
	<?php endwhile; ?> 		
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi( array( 'query' => $menucn_posts ) ); } ?>	
	<?php wp_reset_query(); ?>				
</div><!-- end primary-row --> 