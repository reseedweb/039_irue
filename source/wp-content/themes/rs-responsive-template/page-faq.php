<?php get_template_part('header'); ?>
<div class="primary-row clearfix">
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<div id="faq1">
		<?php $aqs = array(
			array(
				'q' => 'Question 01',
				'a' => 'Answer 01'
			),
			array(
				'q' => 'Question 02',
				'a' => 'Answer 02'
			),
			array(
				'q' => 'Question 03',
				'a' => 'Answer 03'
			),
			array(
				'q' => 'Question 04',
				'a' => 'Answer 04'
			),
			array(
				'q' => 'Question 05',
				'a' => 'Answer 05'
			),												
		); ?>
		<h2><span><?php the_title(); ?></span></h2>
		<div class="faq1-content">
			<?php foreach($aqs as $aqs_item) : ?>			
			<div class="faq1-row">
				<h3><?php echo $aqs_item['q']; ?></h3>
				<div class="faq1-row-content">
					<?php echo $aqs_item['a']; ?>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	<?php endwhile; endif; ?>
</div>
<?php get_template_part('footer'); ?>