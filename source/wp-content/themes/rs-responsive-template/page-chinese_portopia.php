<?php get_header(); ?>
    <div class="primary-row container clearfix"><!-- begin container -->                    
        <div class="row clearfix">
            <div class="col-lg12 col-md-12 col-sm-12 col-xs-12">
                <div class="workshop-main1">										
					<h3 class="workshop-main1-title">好好 中華 ポートピア店</h3>
					<div class="workshop-main1-text1">
						<p>リーズナブルに、本格的な中国粥や飲茶が食べられて、店内で食事していると、まるで「本場」で食事をしている気分になれます。<br />
						飲茶類は数種類を一度に楽しめ、セットの炒飯もぱらっと仕上がっていて最高です。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row container clearfix"><!-- begin container -->                    
		<div class="row clearfix">
			<div class="col-lg6 col-md-6 col-sm-6 col-xs-12 mb20">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/portopia_content_top.jpg" alt="ポートピア店" /></p>
			</div>

            			<div class="col-lg6 col-md-6 col-sm-6 col-xs-12">
				<h4 class="workshop-title1">街を歩けば</h4>
				<div class="workshop-text1">
					<p>ポートアイランドにはみなとじま駅や ラヴィマーナ神戸 ・神戸市立青少年科学館 等、様々なスポットがあります。 <br />
					また、ポートアイランドには、「神戸花鳥園」もあり、「神戸花鳥園」は、鳥類コレクター加茂元照が手がけるテーマパークで、神戸市中央区のポートアイランドにあります。<br />
					温室には色鮮やかな熱帯スイレンを始め数多くの展示植物があり、数多くの鳥が放し飼いされている園内には、鳥とのふれあいが随所でできます。<br />
					全天候型テーマパークで、雨天時でも花の中で鳥と遊べる施設です。このポートアイランドにあるのが、中華料理「好好 中華」です。</p>
				</div>
				<!-- <h5 class="workshop-title2">交通手段</h5>
				<div class="workshop-text2">
					<p>JR元町駅南口より徒歩、元町駅（阪神）から194m</p>
				</div>
				<h5 class="workshop-title2">料理内容</h5>
				<div class="workshop-text2">
					<p>中華料理、飲茶・点心</p>
				</div>
				<h5 class="workshop-title2">営業時間</h5>
				<div class="workshop-text2">
					<p>10：30～21：30　ランチ営業、日曜営業（定休日水曜日）</p>
				</div>
				<h5 class="workshop-title2">予算</h5>
				<div class="workshop-text2">
					<p>￥1,000～￥1,999</p>
				</div> -->
			</div>
			
		</div>

			<div class="col-lg12 col-md-12">
				<table class="company-table-content">
					<tbody>
						<tr>
							<th>店名</th>
							<td>好好中華(ハウハウチュウカ)</td>
						</tr>
						<tr>
							<th>所在地</th>
							<td>〒650-0046<br>兵庫県神戸市中央区港島中町3-2-6 グルメポートアイランド店1F</td>
						</tr>
						<tr>
							<th>TEL</th>
							<td>078-302-5721</td>
						</tr>
						<tr>
							<th>料理内容</th>
							<td>中華料理、飲茶・点心</td>
						</tr>
						<tr>
							<th>交通手段</th>
							<td>
							<p>神戸新交通ポートアイランド線みなとじま駅 徒歩5分</p>
							</td>
						</tr>
						<tr>
							<th>営業時間</th>
							<td>10：30～21：30　ランチ営業、日曜営業（定休日水曜日）</td>
						</tr>
						<tr>
							<th>予算</th>
							<td>￥1,000～￥1,999</td>
						</tr>
					</tbody>
				</table>

				<div class="workshop-map" id="map_area">
					<script>
						$(function() {
							map_insert (34.669405,135.210953);
						});
					</script>
				</div>
			</div>
	</div><!-- end primary-row --> 
	
	<div class="primary-row container clearfix"><!-- begin container -->                    
		<div class="row clearfix">
            <div class="col-md-12">
				<h3 class="workshop-slider-title">好好 中華の特徴</h3>
				<?php get_template_part('part','featurecn');?>
			</div>						
		</div> 
	</div><!-- end primary-row --> 
	
	<?php get_template_part('part','menucn') ;?>   
	                                                    		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>