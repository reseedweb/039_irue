	<div class="workshop-slider">					
		<?php	
		$featurecn_posts = get_posts(array(
		'post_type'=> 'featurecn',				
		));?>			
						
		<?php
			$i = 0;
			foreach($featurecn_posts as $post):
		?>	
		<?php $i++; ?>
		<div>
		<?php 
		if ( has_post_thumbnail() ) {
			$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
			echo '<a rel="lightbox" href="' . $large_image_url[0] . '" title="' . the_title_attribute( 'echo=0' ) . '" >';
			echo '<img src="'. $large_image_url[0].'" title="' . the_title_attribute( 'echo=0' ) . '">';
			echo '</a>';
		}
		?>
		</div><!-- end item -->            
		<?php endforeach; ?>
	</div>